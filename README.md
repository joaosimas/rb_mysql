# README #


# Ruby -- Cucumber + Mysql #


Antes de mais nada, crie uma base de dados para voc� realizar seus testes.

Nosso caso vai ser bem parecido, provavelmente vai mudar apenas o :database, se quiser utilizar "joao" tudo bem.

# Exemplo # 

:host => "localhost", :username => "root", :password => "", :database => "joao"

 
No exemplo do meu banco tenho 1 tabela com o nome de [teste] e tenho 2 colunas [nome e codigo].
OBS: Insira algum registro para ver o resultado do teste.


Na pasta [functions] existem alguns m�todos (selection, inserting, updating e deleting).
Eles s�o respons�veis por orquestrar os testes.


# Vamos falar deste : #

    def selection()

	client = Mysql2::Client.new(:host => "localhost", :username => "root", :password => "", :database => "joao")
    
  	results = client.query("SELECT * FROM teste")
    
  	results.each do |row|
    
  	  puts row["codigo"]
      
      puts row["nome"]
      
    end
   
    client.close
   
    end


A 1� linha � a string de conex�o com o banco, criei a vari�vel [client] para guardar a mesma.

A 2� linha eu crio a vari�vel [results] para receber o que est� guardado em client que � a conex�o com o banco.
.query � para fazer algum tipo de opera��o no banco que no nosso caso � o select acima.

A 3� linha eu pego o retorno do banco [results], nele j� vai estar guardado o select, retornando o conte�do de [nome e codigo].
esse |row| � para percorrer as colunas do banco de dados, no nosso caso ele percorre ["codigo"] e ["nome"] e imprimi o conte�do que
l� estiver.

Eu poderia percorrer apenas a coluna ["codigo"] ou ["nome"] ou qualquer outra coluna que existir.

E por �ltimo apenas fechamos a conex�o com o banco.


# Dentro da pasta step_definitions temos o arquivo db.rb #

    Dado(/^select$/) do

    selection

    end


Ele � o respons�vel por chamar a rotina de teste.


Quando a gente chama o teste

cucumber -t@mysql

Ele vai retornar o cen�rio de teste seguido de codigo e nome.


# � poss�vel utilizar o httparty para testar servi�os, utilizar o capybara para testar seu front #

# Em breve vou criar um artigo simples para eles. #







