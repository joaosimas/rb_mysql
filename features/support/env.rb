require "httparty"
require "httparty/request"
require "httparty/response/headers"
require "cpf_faker"
require "faker"
require "yaml"
require "rspec"
require 'mysql2' 


#Ignore SSL certificate for API requests
HTTParty::Basement.default_options.update(verify: false)

$profile = ENV['PROFILE']